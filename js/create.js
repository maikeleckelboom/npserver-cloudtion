$(document).ready(function radioListener() {

    $.preloadImages = function () {
        for (var i = 0; i < arguments.length; i++) {
            $("<img />").attr("src", arguments[i]);
        }
    }

    $.preloadImages(
        "files/servers/linux(1).svg",
        "files/servers/centos-logo.png",
        "files/servers/ubuntu-logo.png",
        "files/servers/mariadb-logo.png"
    );

    let linux = $('#linux');
    let centos = $('#centos');
    let ubuntu = $('#ubuntu');
    let mariadb = $('#mariadb');

    $('input').on('click', function () {
        if (linux.is(':checked')) {
            // console.log('You have Checked Linux');
            $(".output-container").html("<img src='files/servers/linux(1).svg' class='server-logo linux'><span>Linux Server N3</span>");
        } else if (centos.is(':checked')) {
            // console.log('You have Checked Centos');
            $(".output-container").html("<img src='files/servers/centos-logo.png' class='server-logo centos'><span>CentOS Server N3</span>");
        } else if (ubuntu.is(':checked')) {
            // console.log('You have Checked ubuntu');
            $(".output-container").html("<img src='files/servers/ubuntu-logo.png' class='server-logo ubuntu'><span>Ubuntu Server N4</span>");
        } else if (mariadb.is(':checked')) {
            // console.log('You have Checked Mariadb');
            $(".output-container").html("<img src='files/servers/mariadb-logo.png' class='server-logo mariadb'><span>MariaDB Server N4</span>");
        } else {
            $(".output-container").html("&nbsp");
        }
    });

});