<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="js/vendor/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/plugins/CSSPlugin.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/easing/EasePack.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/TweenLite.min.js"></script>
<link rel="icon" href="files/cloud.svg" type="image/png" sizes="16x16">
<link rel="stylesheet" type="text/css" href="foundation/css/foundation.min.css">
<link rel="stylesheet" type="text/css" href="css/app.css">