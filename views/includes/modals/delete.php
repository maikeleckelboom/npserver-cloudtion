<?php
require_once('../../../php/AccountManager.php');
if (!AccountManager::modalPageLoginCheck()) {
    die('Unauthorized modal access.');
}
?>

<div class="modal-container">
    <div class="grid-x grid-padding-x grid-padding-y">

        <div class="cell small-24 medium-12">

            <h4>Verwijder Server</h4>

            <p>
                De server uitgezet wordt en daarna verwijderd. Na de annulering ben je alle data
                kwijt
                die jij op de server had opgeslagen.
            </p>

            <span class="call warning">Het is niet mogelijk om de annulering weer ongedaan te maken</span>

            <span id="close-button" data-close>
                <span>Ga terug</span>
            </span>

        </div>


        <div class="cell small-24 medium-12">

            <p class="callout border">
                Hier annuleer jij de server die jij momenteel in gebruik hebt.
            </p>

            <div class="grid-x grid-margin-y grid-padding-y delete-visual">
                <div class="cell small-14 vi-1">
                    <div class="option-container">
                        <img src='files/servers/linux(1).svg' class='server-logo linux'><span>Linux Server N3</span>
                    </div>

                </div>
                <div class="cell small-5">
                    <img src="files/right-arrow-of-straight-thin-line.svg" class="vi-2">
                </div>
                <div class="cell small-5">
                    <img src="files/delete(1).svg" class="vi-3">

                </div>
            </div>

            <div class="button-group">
                <button class="delete-btn verwijderServer" id="delete">
                    <span>Verwijder server</span>
                    <?php include '../../../files/angle-right.html'; ?>
                </button>
            </div>


        </div>


    </div>
</div>

<script>
    $(document).ready(function () {
        // Spinner button
        let spinnerButton;
        return spinnerButton = new SpinnerButton($(".verwijderServer"),
            () => setTimeout(() => spinnerButton.stop(), 1000));
    });
</script>