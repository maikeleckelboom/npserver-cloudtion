<?php
require_once('../../../php/AccountManager.php');
if (!AccountManager::modalPageLoginCheck()) {
    die('Unauthorized modal access.');
}
?>
<div class="modal-container">
    <div class="grid-x grid-padding-x grid-padding-y">

        <div class="cell small-24 medium-10 modal-right">

            <h4>Reserveer Server</h4>

            <p>
                Hier maak je een nieuwe server aan. Kies het type dat je wilt en kies een gebruikersnaam en wachtwoord.
            </p>

            <span id="close-button" data-close>
                <svg width="100%" height="100%" viewBox="0 0 11 20" version="1.1" class="angle-left">
                    <path style="fill:none;stroke-width: 1px;stroke: #000;"
                          d="M1.054,18.214l8.606,-8.606l-8.606,-8.607"/>
                </svg>
                <span>Ga terug</span>
            </span>

            <div class="cell small-12">
                <div class="requirements-container">
                    <!-- Requirements -->
                    <div class="requirements useraccount">
                        <ul>
                            <li>Alleen letters</li>
                            <li>Geen cijfers</li>
                            <li>Geen leestekens</li>
                            <li>Maximaal 8 tekens</li>
                            <li>Mag niet student zijn</li>
                            <li>Mag niet root zijn</li>
                            <li>Mag niet dba zijn</li>
                        </ul>
                    </div>

                    <!-- Requirements -->
                    <div class="requirements userpassword">
                        <ul>
                            <li>Onthoud dit wachtwoord</li>
                            <li>Gebruik een uniek wachtwoord</li>
                            <li>Wachtwoord wordt ook in tekst opgeslagen op server</li>
                            <li>Gebruik geen wachtwoord die je voor andere services gebruikt</li>
                            <li>Deze kun je vinden in de bevestigingsemail</li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>

        <div class="cell small-24 medium-14">

            <form class="form-leader" id="createForm">
                <div class="grid-y grid-padding-y">

                    <div class="cell small-24">
                        <table class="unstriped server-choices">
                            <tr>
                                <td colspan="2">
                                    <div class="callout border">
                                        <b>Stap 1</b>
                                        <p>Kies het type server dat je wilt aanmaken</p>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="radio-container">
                                        <input type="radio" id="linux" name="radio-group">
                                        <label for="linux">Linux - N3</label>
                                    </div>
                                </td>
                                <td>
                                    <div class="radio-container">
                                        <input type="radio" id="centos" name="radio-group">
                                        <label for="centos">CentOS Linux - N4</label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="radio-container">
                                        <input type="radio" id="ubuntu" name="radio-group">
                                        <label for="ubuntu">Ubuntu Linux - N4</label>
                                    </div>
                                </td>
                                <td>
                                    <div class="radio-container">
                                        <input type="radio" id="mariadb" name="radio-group">
                                        <label for="mariadb">MariaDB - N4</label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="output-container">

                                    </div>
                                </td>

                            </tr>
                        </table>
                    </div>
                    <div class="cell small-24">
                        <div class="form-division">
                            <div class="callout border">
                                <b>Stap 2</b>
                                <p>Kies een gebruikersnaam en wachtwoord voor deze server</p>
                            </div>
                            <div class="form-group">
                                <label for="useraccount">Gebruikersnaam</label>
                                <input type="text"
                                       id="useraccount"
                                       placeholder="Gebruikersnaam"
                                       name="password"
                                       data-rule="required|username">
                            </div>

                            <div class="form-group">
                                <label for="userpassword">Wachtwoord</label>
                                <input type="text"
                                       id="userpassword"
                                       placeholder="Wachtwoord"
                                       name="password"
                                       data-rule="required|password">
                            </div>
                        </div>

                        <div class="button-group">
                            <button class="reserveerServer">
                                <span>Reserveer server</span>
                                <?php include '../../../files/angle-right.html'; ?>
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="js/create.js"></script>
<script>
    $(document).ready(function () {
        // Button
        let spinnerButton;
        return spinnerButton = new SpinnerButton($(".reserveerServer"), () => {
            return res;
        });
    });
    $(document).ready(function () {
        // Validator
        new Validator(document.querySelector('#createForm'), function (err, res) {
            $('#createForm').submit();
        });
    });

    // Requirements
    $(document).ready(function () {
        $('.form-group #useraccount').focus(function () {
            $('.requirements.useraccount').fadeIn(200);
        }).focusout(function () {
            $('.requirements.useraccount').fadeOut(200);
        });
        $('.form-group #studentname').focus(function () {
            $('.requirements.studentname').fadeIn(200);
        }).focusout(function () {
            $('.requirements.studentname').fadeOut(200);
        });
        $('.form-group #studentemail').focus(function () {
            $('.requirements.studentemail').fadeIn(200);
        }).focusout(function () {
            $('.requirements.studentemail').fadeOut(200);
        });
        $('.form-group #userpassword').focus(function () {
            $('.requirements.userpassword').fadeIn(200);
        }).focusout(function () {
            $('.requirements.userpassword').fadeOut(200);
        });
    });
    // Username regular expression
    // ^(?=.*[a-zA-Z])(?!.*[0-9#$^+=!*()@%&]).{2, 8}$
</script>

<!--                                        <img src='files/servers/linux(1).svg' class='server-logo linux'><span>Linux Server N3</span>-->
<!--                                        <img src='files/servers/centos-logo.png' class='server-logo centos'><span>CentOS Server N3</span>-->
<!--                                        <img src='files/servers/ubuntu-logo.png' class='server-logo ubuntu'><span>Ubuntu Server N4</span>-->
<!--                                        <img src='files/servers/mariadb-logo.png' class='server-logo mariadb'><span>MariaDB Server N4</span>-->