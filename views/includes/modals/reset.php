<?php
require_once('../../../php/AccountManager.php');
if (!AccountManager::modalPageLoginCheck()) {
    die('Unauthorized modal access.');
}
?>

<div class="modal-container">
    <div class="grid-x grid-padding-x grid-padding-y">

        <div class="cell small-24 medium-12">

            <h4>Reset Server</h4>

            <p>
                De server wordt gereset. Na de reset ben je alle data kwijt
                die jij mogelijkerwijs op de server had opgeslagen.
            </p>

            <span id="close-button" data-close>
                <span>Ga terug</span>
            </span>

        </div>

        <div class="cell small-24 medium-12">


            <div class="cell small-24 medium-12">

                <p class="callout border">
                    Hier reset jij de server en verwijder je alle opgeslagen data
                </p>

                <div class="grid-x grid-margin-y grid-padding-y delete-visual">
                    <div class="cell small-12 vi-1">
                        <div class="option-container">
                            <img src='files/servers/linux(1).svg' class='server-logo linux'><span>Linux Server N3</span>
                        </div>

                    </div>
                    <div class="cell small-6">
                        <img src="files/right-arrow-of-straight-thin-line.svg" class="vi-2">
                    </div>
                    <div class="cell small-6">
                        <img src="files/reset(1).svg" class="vi-3">

                    </div>
                </div>

                <div class="button-group">
                    <button class="delete-btn resetServerModal">
                        <span>Reset server</span>
                        <?php include '../../../files/angle-right.html'; ?>
                    </button>
                </div>

            </div>


        </div>

    </div>
</div>

<script>
    // Spinner button
    $(document).ready(function () {
        let spinnerButton;
        return spinnerButton = new SpinnerButton($(".resetServerModal"),
            () => setTimeout(() => spinnerButton.stop(), 1000));
    });
</script>